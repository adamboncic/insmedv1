$(document).ready(function() {


  (function($) {

    // tabs
    var tabs = $(".objections");

    // indexing the tabs
    tabs.each(function(index) {
      console.log(index, $(this));
    });

    // active / inactive
    tabs.click(function() {
      // new tab
      var active = $(this).attr('data-tab');
      // old tab answer
      var answer = $('#' + active + ' .answer-bn').attr('data-answer');
      // old tab question
      var question = $('#' + active + ' .answer-bn').attr('data-question');
      if (!$(this).hasClass('active_nav')) {
        // newly clicked tab
        $(this).addClass('active_nav');
        // removing active from prev tab
        $('.active_nav').not($(this)).removeClass('active_nav');
        // resetting questions for prev tab
        $('.' + answer).addClass('hidden');
        $('.' + question).removeClass('hidden');
        // old tab
        $('.active_tab').removeClass('active_tab');
        // new tab
        $('#' + active).addClass('active_tab');
      }
    });

  })(jQuery);

  $('.answer-bn').click(function() {
    console.log('test');
    var answer = $(this).attr('data-answer');
    var question = $(this).attr('data-question');
    $('.' + question).addClass('hidden');
    $('.' + answer).removeClass('hidden');
    console.log(answer);
    console.log(question);
  });


  $('.admin-link').click(function(){
    $('.active_nav').removeClass('active_nav');
    $('.active_tab').removeClass('active_tab');
    $('#obj .objections:first-of-type, #obj-cer .objections:first-of-type').addClass('active_nav');
    $('#obj .obj_tab:first, #obj-cer .obj_tab:first').addClass('active_tab');

    $('#signin_holder').removeClass('hidden');
    $('#admin_panel').addClass('hidden');
  })


  // (function($) {
  //
  //   // tabs
  //   var li = $(".deck-list a");
  //
  //   // indexing the tabs
  //   li.each(function(index) {
  //     console.log(index, $(this));
  //   });
  //
  //   li.click(function() {
  //
  //     var active = $(this).attr('a');
  //     $('#' + active).
  //
  //   });
  //
  // });

});
