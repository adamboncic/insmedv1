      /*
        // Note from Cat //
        I'm adding this to make a function for the pop ups.
        what happens is I will use the id as a key and then make a json item with the
        paramaters (image url, title text if it exists and height and width so it can be changed easily)

      */

      var popUpData = {
        //Branch 1 - Tab 1
        "zoom-1-c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/branch1-spread1-1"
        },
        "zoom-2-c": {
          "height": 3000,
          "width": 814,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/branch1-spread1-2.jpg"
        },
        "pop-bn-1-c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" +
              "<h3>Context (for your background information):</h3>  This is the first part of your core clinical story, where you introduce the product." + 
              "</br></br></br><h3>Key Points:</h3><ul><li>ARIKAYCE is the first and only FDA-approved therapy indicated for the treatment of nontuberculous" +  
              "mycobacterial (NTM) lung disease caused by <em>Mycobacterium avium</em> complex (MAC) as part of a combination antibiotic regimen for adult patients </li>" + 
              "<li>This indication is approved under accelerated approval based on achieving sputum culture conversion (defined as 3 consecutive negative monthly " + 
              "sputum cultures) by Month 6. Durability of sputum culture conversion at 3-months after cessation of therapy has not yet been established." + 
              " Continued approval for this indication may be contingent upon verification and description of clinical benefit in confirmatory trials</li></ul>" +
            "</div>"
        },
        "pop-bn-2-c": {
          "height": 650,
          "width": 1410,
          "left": 0,
          "top": 8,  
          "bottom": 0,
          "right": 0,  
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/2-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 2%;'>" +
          "<h3>Context (for your background information):</h3> Remember, although this is on one of the 1st pages of the PI, this is the 6th part of your core clinical call. " + 
          "</br></br>During market research, HCPs commonly had questions about ARIKAYCE administration.  If questions arise, remind HCPs that ARIKAYCE can <b>only</b> be administered " + 
          "with the Lamira Nebulizer System (no other systems can be used), and that patients receive a Lamira Nebulizer System as part of their first shipment. Patients do not need" + 
          " to seek separate reimbursement for the system and administration of the product can be done at home. Patients should review and have a complete understanding of the Instructions for Use." + 
          "</br></br><h3>Key Points:</h3><ul><li>The recommended dose of ARIKAYCE in adults is the once-daily inhalation of the contents of one ARIKAYCE vial using the Lamira Nebulizer System</li>" + 
          "<li>ARIKAYCE should be administered only by oral inhalation via the Lamira Nebulizer System</li>" +
          "<li>ARIKAYCE should be administered around the same time each day for approximately 14 minutes, but could take up to 20 minutes </li>" + 
          "<li>ARIKAYCE can be taken at home</li><li>After the first culture conversion is achieved, treatment should be continued for an additional 12 months of negative sputum cultures while on therapy</li></ul>" +
        "</div>" + "<img id='qr-code' src='img/Workshop1_1 - SL101.png'>"
        },
        "pop-bn-3-c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/3-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 5%;'>" + 
          "<h3>Context (for your background information):</h3> You'll learn more about the Lamira Nebulizer System on Wednesday during that" + 
          " workshop, as those details are not part of our core clinical story. As a little more context now, a new patient will recieve " + 
          "Lamira Nebulizer System and support information in addition to ARIKAYCE 28-day pack in his/her 1st shipment" +
          "<img style='margin: 0 auto; margin-top: 59px; box-shadow: none;' src='img/M1Sc17.png'>" + "</div>"
        },
        // Branch 1 - Tab 2
        "zoom-1-2c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread2-1.jpg"
        },
        "zoom-2-2c": {
          "height": 3000,
          "width": 1660,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread2-2.jpg"
        },
        "zoom-3-2c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread2-3.jpg"
        },
        "pop-bn-2-1c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "Although not specifically called out in our core clinical story, it’s important for you to know that people who are allergic to aminoglycoside should not take ARIKAYCE (which is an aminoglycoside), as listed here in Section 4." + "</div>"
        },
        "pop-bn-2-2c": {
          "height": 650,
          "width": 1410,
          "left": 0,
          "top": 8,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/2-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 3%;'>" + "<h3>Context (for your background information):</h3>" + 
          "This section of the PI contains all the important safety considerations to communicate to HCPs.  Ototoxicity and nephrotoxicity are likely to be top of mind for HCPs as they are reported or associated with parenteral aminoglycosides." +
          "<ul><li>Ototoxicity, manifested as both auditory (hearing loss) and vestibular toxicity, has been associated with" + 
          "parenteral aminoglycosides. Ototoxicity, as measured by reports of deafness, hypacusis, tinnitus, and vertigo, were reported during ARIKAYCE" + 
          "clinical studies and has been reported with parenteral aminoglycosides. In the CONVERT clinical trial, tinnitus and dizziness were reported at a" + 
          "higher frequency in patients in the ARIKAYCE + MDR arm</li>" + 
          "<li id='pop-bn-large-modal-indent'>A total of 17 (7.6%) patients experienced 20 events of tinnitus in the ARIKAYCE + multidrug regimen arm (MDR)" + 
          " compared to 1 (0.9%) patient in the MDR arm alone. Most cases of tinnitus were mild in severity.  The study drug was not interrupted in 13 out " + 
          "of 20 events, was interrupted in 6 out of 20 events, and was withdrawn for 1 event</li>" + 
          "<li id='pop-bn-large-modal-indent'>A total of 14 (6.3%) of patients in the ARIKAYCE + MDR arm experienced 15 TEAEs of dizziness compared to 3 " + 
          "(2.7%) patients and 3 events in the MDR alone arm.  All events were nonserious with most events in the ARIKAYCE + MDR arm of mild severity (11/15). " + 
          "Four events were of moderate severity and study drug was interrupted in 2 patients, with all events of moderate severity resolved </li>" + 
          "<li>Across ARIKAYCE clinical studies, nephrotoxicity was reported similarly in the ARIKAYCE and control arms with a frequency of <5%</li>" + "</div>" +
          "<img id='qr-code' src='img/Workshop1_2 - SL102.png'>"
        },
        // Branch 1 - Tab 3
        "zoom-1-3c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread3-1.jpg"
        },
        "zoom-2-3c": {
          "height": 3000,
          "width": 1660,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread3-2.jpg"
        },
        "zoom-3-3c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread3-3.jpg"
        },
        "pop-bn-3-3c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,   
          "background-img": "img/3-modal-small.png",
          "html": "<div id='pop-bn-small-modal' style='top: 13%;'>" + "<h3>Context (for your background information):</h3>" + 
          "It’s important to discuss these safety considerations with doctors. Table 1 summarizes all the Treatment Emergent Adverse Events (TEAEs) from most to least frequent.</br>" + 
          "</br>Although the PI does not specifically state the severity of these TEAEs, the COVERT INS-212 clinical study does include this information and it is part of our core story messaging." +
          "</br></br><h3>Key Points:</h3>" + "<ul><li>Most TEAEs occurring in both treatment arms were respiratory in nature</li>" + 
          "<li>Most common AEs in patients treated with ARIKAYCE + MDR were dysphonia, cough, dyspnea, hemoptysis, and fatigue</li>" + 
          "<li>Most TEAEs were mild to moderate in nature and did not lead to withdrawal. In the ARIKAYCE + MDR arm, 39 out of 224 patients discontinued due to adverse events</li></ul>" + "</div>"
        },
        "pop-bn-3-4c": {
          "height": 650,
          "width": 1410,
          "left": 0,
          "top": 8,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/4-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 6%;'>" + "<h3>Context (for your background information):</h3>" + 
          "In market research and advisory board, cough was commonly asked about.  Make sure to review this section and be prepared to answer questions:" + 
          "<ul><li>There was a higher rate of cough adverse event reporting during the first month of active treatment with ARIKAYCE but thereafter the incident " + 
          "frequency of cough event in the ARIKAYCE + multidrug regimen and multidrug regimen alone groups were similar </li>" + 
          "<li>The majority of episodes of cough lasted less than one minute with most episodes lasting less than 10 minutes</li></ul>" + 
          "Additionally, this section of the PI specifically mentions cough was reported with the highest frequency in the first study month following initiation " + 
          "of ARIKAYCE treatment.  However, the COVERT INS-212 clinical study also states that dysphonia, oropharyngeal pain, dyspnea, diarrhea, nausea, " + 
          "and fatigue were reported with the highest frequency in the first study month following initiation of ARIKAYCE treatment.  Make sure to verbalize this " + 
          "full list as part of our core story." + 
          "<h3>Key Points:</h3>" + "<ul><li>In patients treated with ARIKAYCE, cough, dysphonia, oropharyngeal pain, dyspnea, diarrhea, nausea, and fatigue were" + 
          "reported with the highest frequency in the first study month following initiation of ARIKAYCE treatment</li></ul>" +  "</div>"
        },
        // Branch 1 - Tab 4
        "zoom-1-4c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread4-1.jpg"
        },
        "zoom-2-4c": {
          "height": 3000,
          "width": 1660,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread4-2.jpg"
        },
        "zoom-3-4c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread4-3.jpg"
        },
        "pop-bn-4-5c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/5-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "It’s also important to discuss these specific populations with doctors." + "</div>"
        },
        // Branch 1 - Tab 5
        "zoom-1-5c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread5-1.jpg"
        },
        "zoom-2-5c": {
          "height": 3000,
          "width": 1735,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread5-2.jpg"
        },
        "pop-bn-5-1c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png", 
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "This is the 2<sup>nd</sup> part of the core story, focusing on the MOA and PULMOVANCE technology of ARIKAYCE." + 
          "</br></br><h3>Key Points:</h3>" + "<ul><li>ARIKAYCE is an inhaled liposomal formulation of amikacin that allows high peaks of drug concentration " + 
          "to be delivered to the site of infection, while minimizing systemic exposure and potential toxicities</li>" + 
          "<li>ARIKAYCE rapidly permeates through human sputum and can penetrate biofilms</li>" + 
          "<li>ARIKAYCE delivers amikacin to the site of biofilm and intracellular mycobacterial infections by extending the time amikacin remains" + 
          "in the lungs and improving amikacin uptake into the macrophages</li></ul>" + "</div>" + "<img id='qr-code' src='img/Workshop1_3 - SL103.png'>"
        },
        // Branch 1 - Tab 6
        "zoom-1-6c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread6-1.jpg"
        },
        "zoom-2-6c": {
          "height": 3000,
          "width": 1735,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread6-2.jpg"
        },
        "zoom-3-6c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread6-3.jpg"
        },
        "pop-bn-6-2c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/competition graphic_large.png",
          "html": "<div id='pop-bn-small-modal'>" + "</div>" + "<img id='qr-code' src='img/Workshop1_4 - SL104.png'>"
        },
        // Branch 1 - Tab 7
        "zoom-1-7c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread7-1.jpg"
        },
        "zoom-2-7c": {
          "height": 3000,
          "width": 1735,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread7-2.jpg"
        },
        "zoom-3-7c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread7-3.jpg"
        },
        "pop-bn-7-1c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,    
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "This is the 3rd part of our core clinical story and below are the important key points about the study design." + 
          "</br></br><h3>Key Points:</h3>" + "<ul><li>Ongoing, open-label, randomized (2:1), multi-center, global, Phase 3 trial of 336 adult patients</li>" +
          "<li>Patients had nontuberculous mycobacterial (NTM) lung disease caused by <em>Mycobacterium avium</em> Complex (MAC) and were not responding treatment as confirmed by at least 2 sputum culture results</li>" + 
          "<li>Primary endpoint was based on achieving culture conversion by Month 6</li>" + 
          "<li>Overall, at baseline, 54.9% of subjects were receiving a triple multidrug regimen (MDR) of a macrolide, a rifamycin and ethambutol</li>" + "</ul>" + "</div>"
        },
        "pop-bn-7-2c": {
          "height": 650,
          "width": 1498,
          "left": -8,
          "top": 5,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/2-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 1%;width: calc(100% - 25rem);'>" + "<h3>Context (for your background information):</h3>" +
          "This is the 4th part of our core clinical story – our efficacy data.</br></br>" +
          "This chart visually represents the primary endpoint.  If you look at the purple and gray bars on the far right " + 
          "side of the graph, it shows the proportion of subjects achieving culture conversion was significantly greater for " + 
          "ARIKAYCE + multidrug regimen (purple bar = 29.0%) compared to multidrug regimen alone (gray bar = 8.9%).</br></br>" + 
          "The chart shows baseline through Month 4 data because culture conversion was defined as 3 consecutive monthly negative " + 
          "sputum cultures, and the <b>time of conversion</b> was defined as the <b>first</b> of those negative sputum cultures. " + 
          "So if a patient was considered as culture converted by Month 6, they had to have produced negative sputum cultures " + 
          "in Month 4, 5, and 6. The <b>first</b> of those consecutive negative sputum cultures occurred in Month 4. In market research, " + 
          "this concept required clarification with some HCPs, so make sure to be prepared to explain this chart and why it only shows data through Month 4.</br></br>" + 
          "Another question that arose in market research was why this chart shows some conversion at Baseline (the far left hand side of the chart) and this is due " + 
          "to the 10-week window between screening and baseline. While patients had to have a positive sputum culture for MAC at screening, some patients produced " + 
          "their first negative culture at the baseline visit." + 
          "<h3>Key Points:</h3>" + "<ul><li>In the CONVERT trial, the proportion of patients who achieved culture conversion by Month 6 was significantly greater" + 
          " in the ARIKAYCE + multidrug regimen arm versus the multidrug regimen alone arm (29% vs. 8.9%)</li></ul>" + "</div>" +
          "<img style='right:-13%;' id='qr-code' src='img/Workshop1_5 - SL105.png'>" +
          "<img style='right:-13%;' id='qr-code' src='img/PLACEHOLDER QR.png'>"
        },
        "pop-bn-7-3c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "background-img": "img/3-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 5%;'>" + "<h3>Context (for your background information):</h3>" +
          "This Kaplan-Meier curve shows the time to culture conversion for both the ARIKAYCE + MDR, and the MDR alone arms. " + 
          "This differs from the previous chart which shows cumulative proportion of patients achieving culture conversion by month of first conversion." + 
          "</br></br>Below the chart, it mentions our secondary endpoint of the 6-minute walk test (6MWT) and aligns with the additional messaging in the 4th part of our core clinical story. " + 
          "6MWT may not be commonly used in clinical practice for patients with NTM, so it is important to appropriately ask probing questions to understand the HCP's familiarity with the 6MWT in this context." +
          "</br></br><h3>Key Points:</h3>" + "<ul><li>There was no statistically significant difference between treatment arms in secondary endpoint of change from baseline in 6-minute walk test (6MWT) at Month 6</li>" + 
          "<li>However, the exploratory endpoint comparing the 6MWT distance showed an improvement among patients who converted over non-converters regardless of treatment arm</li>" + "</ul>" + "</div>"
        },
        "pop-bn-7-4c": {
          "height": 650,
          "width": 1450,
          "left": 0,
          "top": 5,  
          "bottom": 0,
          "right": 0,   
          "background-img": "img/4-modal-small.png",
          "html": "<div id='pop-bn-large-modal' style='top: 4%; right:3%; width:calc(100%-25rem);'>" + "<h3>Context (for your background information):</h3>" + 
          "This page aligns to the last part of our efficacy core clinical story messaging and discusses the extension study.</br></br>" +
          "Any participant in CONVERT who didn’t achieve the primary endpoint (or who had a relapse or recurrence by Month 6), was " + 
          "eligible to be enrolled in this extension study. In this extension study, all participants were given ARIKAYCE + MDR. " + 
          "This was NOT a comparison study." + 
          "<ul><li>That means if a participant was in the initial ARIKAYCE + MDR arm of CONVERT, he/she continued with that treatment.  In an interim analysis, 5.1% of these extension study participants went on to achieve culture conversion by Month 6 by continuing on ARIKAYCE + MDR.</li>" +
          "<li>If a participants was in the MDR only arm of CONVERT, he/she started ARIKAYCE + MDR for the first time. 23.0% of these extension study participants achieved culture conversion in an interim analysis.</li>" + 
          "</ul>" + "<h3>Key Points:</h3>" + 
          "<ul><li>In INS-312 extension study, the primary endpoint was to evaluate the long-term safety and tolerability of ARIKAYCE<sup>®</sup> 590 mg administered daily (QD) for up to 12 months in patients who were refractory to standard multidrug treatment and failed to convert in CONVERT</li>" + 
          "<li>Based on an interim report from the extension study, a higher percentage of patients from the prior CONVERT trial MDR-alone arm achieved culture conversion by Month 6 than patients in the ARIKAYCE + MDR-arm (23.0% and 5.1%, respectively)</li>" + "</ul>"
          + "</div>"

        },
        // Branch 1 - Tab 8
        "zoom-1-8c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread8-1.jpg"
        },
        "zoom-2-8c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread8-2.jpg"
        },
        "zoom-3-8c": {
          "height": 3000,
          "width": 881,
          "left": 0,
          "top": -5,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/branch1-spread8-3.jpg"
        },
        "pop-bn-8-1c": {
          "height": 650,
          "width": 1320,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0,  
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-modal'>" + "<h3>Context (for your background information):</h3>" + 
          "There is an entire workshop on tomorrow’s agenda dedication to getting hands-on experience with this product " + 
          "and the Lamira Nebulizer System.  This additional level of detail is not a part of our core clinical story and " + 
          "HCP messaging, but is expected to be used during more in-depth discussions with office staff." + "</div>" + "<img id='qr-code' src='img/Workshop1_6 - SL106.png'>"
        },
        "star-bn-pa-1-v": {
          "height": 650,
          "width": 950,
          "left": 0,
          "top": 16,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='right: 10%; top: 12%;'>" + "<ul><li>The Therapeutic Specialist can inform HCP offices that:</li>" + 
          "<li id='pop-bn-small-plain-indent'>PA forms for ARIKAYCE are available on CMM</li>" + 
          "<li id='pop-bn-small-plain-indent'>To contact CMM directly for any questions</li>" + "</ul>" + "</div>"
        },
        "star-bn-pa-3-v": {
          "height": 650,
          "width": 1180,
          "left": 0,
          "top": 13,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='top: 1%; left:10%;'>" + "<ul><li>Arikares Coordinator has visibility into the PA through coverage determination</li>" + 
          "<li>Arikares Coordinator monitors PA progress through manual follow-up with HCP</li>" + 
          "<li>Asembia & Specialty Pharmacy follow-up with payer to confirm receipt/determination</li>" + "</ul>" 
          + "<div id='PA-box'>If a patient is not enrolled in the Arikares Support Program, the PA process is coordinated with the HCP office by" + 
          " the specialty pharmacy. The Arikares Coordinator will not have visibility into PA status through CoverMyMeds.</div>"
          + "</div>"
          + "<img id='qr-code' src='img/Workshop3_9 - SL309.png' style='right: -22%; z-index: 2;'>" 
          + "<img id='qr' src='img/PLACEHOLDER QR.png' style='right: -22%;'>"
        },
        "star-bn-pa-4-v": {
          "height": 650,
          "width": 1450,
          "left": 0,
          "top": 6,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='right: 8%; top: 10%;'>" + 
          "<ul><li style='top: -9%; position: absolute;'>Therapeutic Specialist has visibility into PA submission status of each patient (deidentified within his/her territory through 'Operational Alerts'</li>" +  
          "</ul>" + "<div style='word-wrap: break-word; width: 280px; font-size:27px;position: absolute; z-index: 2; right: -1.3%; top:20%;'>Overall <b>Territory Level</b> summary of Patient Enrollment Status</div>" + 
          "<div style='word-wrap: break-word; width: 314px; font-size:27px; position: absolute; z-index: 2; right: -3.5%; top:62%;'><b>Patient Level</b> Enrollment status with details around BI/BV, PA submission and shipment status</div>" + "<img style='box-shadow: none; height: 82%; width: 106%; margin-top: 63px; position: absolute; left: -18px;' src='img/elrkuhf.png'>" 
          + "</div>"
        },
        "star-bn-pa-5-v": {
          "height": 650,
          "width": 1305,
          "left": 0,
          "top": 11,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='top: -2%;padding:10px;'>" + "<ul><li>The Arikares Coordinator will primarily follow up about prior authorization with an HCP office</li>" + 
          "<li>The Arikares Coordinator may escalate an HCP bottleneck issue to you for follow-up when necessary. You are expected to help out and assist in a timely and compliant manner and report back to the Arikares Coordinator</li>" + 
          "<li>If you identify access challenges that may require follow up with a payer, you should communicate these situations with your Regional Director and KAD</li>" +
          "<li>Remember, you are a member of the field sales team for Insmed, and will not be representing the Patient Services organization. You will, however, follow approved guidelines when discussing Support Program details with customers</li>"
          + "</ul>" + "</div>"
          + "<img id='qr-code' src='img/Workshop3_9 - SL309.png' style='right: -18%; z-index: 2;'>" 
          + "<img id='qr' src='img/PLACEHOLDER QR.png' style='right: -18%;'>"
        },
        "star-bn-pa-6-v": {
          "height": 650,
          "width": 1305,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='top: -7%;padding:19px;'>" + "<ul><li>If PA was denied, the HCP/patient can decide to appeal payer decision. The appeals process is not managed within CoverMyMeds</li>" + 
          "<li>Information available for HCP offices in the HCP Portfolio (see below)</li>" +
          "<img style='width: 24rem; box-shadow: none;' src='img/01 HCP Portfolio Closed.png' alt='hcp closed'>" +
          "<img style='width: 35rem; box-shadow: none; margin-top: -56px;' src='img/04 HCP Portfolio Full Open.png' alt='hcp open'>" +
          "<li>This information is also available on the Arikares section of HCP website</li>"
          + "</ul>" + "</div>"
          + "<img id='qr-code' src='img/Workshop3_10 - SL310.png' style='right: -18%; z-index: 2;'>" 
          + "<img id='qr' src='img/PLACEHOLDER QR.png' style='right: -18%;'>"
        },
        "star-bn-pa-7-v": {
          "height": 650,
          "width": 1305,
          "left": 0,
          "top": 12,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small-plain.png",
          "html": "<div id='pop-bn-small-plain' style='top: 3%;padding:10px;'>" + "The Arikares Coordinator will contact the patient to discuss their approval and financial obligations"
           + "<div id='row1'>" + "<div id='column1'><h3>For Commercial or Private Insurance:</br>ARIKAYCE Co-pay Savings Program*</h3>" + 
           "<ul><li>Eligible patients using private or commercial insurance can save on out-of-pocket costs for ARIKAYCE</li>" + 
           "<li>Most patients who are eligible, pay $0 co-pay every month, up to a $32,000 maximum program benefit limit of $8,000 per month. Patient will be responsible for any co-pay once limit is reached. Depending on the private or commercial health insurance plan, savings may apply toward co-pay, co-insurance, or deductible</li>" +
           "<li>Not valid for prescriptions covered by or submitted for reimbursement under Medicaid, Medicare, VA, DOD, TRICARE or similar federal or state programs, including any state pharmaceutical assistance program</li>" +
           "<li>Eligibility can be determined by calling Arikares at 1-833-ARIKARE (1-833-274-5273).</li>" +
           "<li>*See full terms and conditions</li>" + "</ul>" + "</div>" + 
           "<div id='column2'><h3>For Government Coverage:</h3><ul><li>If you receive questions or concerns about Medicare Part D coverage, use approved resources to discuss financial assistance programs that such patients may be eligible for</li></ul></div>" 
           + "</div>" + "</div>"
        },
        "star-bn-1-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/Branch 2-HCP-Popover1.png",
          "html": "<div class='fragment' data-fragment-index='11' data-autoslide='2000'>" +
          "<button type='button' data-animate='pulseAlt' id='star-bn-1-v1' class='open-popup-btn'></button>" + 
          "</div>" + "<img id='qr-code' src='img/Workshop3_2 - SL302.png'>"
        },
        "star-bn-2-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/Branch 2- HCP-Popover2.png",
          "html": "<img id='qr-code' src='img/Workshop3_4 - SL304.png'>"
        },
        "star-bn-3-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png",
          "background-img": "img/Branch 2-HCP-Popover3.png",
          "html": "<div class='fragment' data-fragment-index='11' data-autoslide='2000'>" +
          "<button type='button' data-animate='pulseAlt' id='star-bn-3-v1' class='open-popup-btn'></button>" + 
          "</div>" +
          "<div class='fragment' data-fragment-index='11' data-autoslide='2000'>" +
          "<button type='button' data-animate='pulseAlt' id='star-bn-3-v2' class='open-popup-btn'></button>" + 
          "</div>" +
          "<div class='fragment' data-fragment-index='11' data-autoslide='2000'>" +
          "<button type='button' data-animate='pulseAlt' id='star-bn-3-v3' class='open-popup-btn'></button>" + 
          "</div>" + "<img id='qr-code' src='img/Workshop3_5 - SL305.png'>"
        },
        "star-bn-4-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/Branch 2- HCP-Popover2.png"
        },
        "star-bn-1-v1": {
          "height": 650,
          "width": 1450,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-large-plain.png",
          "html": "<div id='pop-bn-large-plain' style='top: 0%;'>" + "<p style='font-size: 35px;'>When talking with HCPs, use the following talking points as an overview of the program:</p>" + 
          "<div id='p-box'>" + "<h3><em>The Arikares Support Program supports ARIKAYCE patients from start to finish." + 
          "Once a patient is enrolled in the Arikares Support Program:</em></h3>" +
          "<ul><b><em><li>The Arikares Case Manager can:</li>" + 
          "<li id='pop-bn-large-plain-indent'>Provide assistance with payer navigation, information, prior authorization, and prescription access</li>" +
          "<li id='pop-bn-large-plain-indent'>Help patients appropriately manage their therapy as prescribed for the duration of treatment</li>" +
          "<li id='pop-bn-large-plain-indent'>Will work with an appropriate specialty pharmacy to process and ship ARIKAYCE to a patient's home</li>" + 
          "<li>Arikares Trainers (nurses and respiratory therapists) may provide an in-home training at treatment initiation</li>" +
          "<li>Arikares can provide patients with appropriate encouragement, as well as ongoing support during their course of care</li>" + "</em></b></ul>" + "</div>"
          + "</div>" + "<img style='right:-14%; z-index:2;' id='qr-code' src='img/Workshop3_3 - SL303.png'>" +
          "<img style='right:-14%;' id='qr' src='img/PLACEHOLDER QR.png'>"
        },
        "star-bn-3-v1": {
          "height": 650,
          "width": 950,
          "left": 0,
          "top": 16,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/1-modal-small.png",
          "html": "<div id='pop-bn-small-plain' style='top: 29%; right: 2%;'>" + "The information on this form must be completed by the patient, " + 
          "their healthcare provider, and the office staff. As a Therapeutic Specialist, you should not complete any information " + 
          "on this form." + "</div>"
        },
        "star-bn-3-v2": {
          "height": 650,
          "width": 950,
          "left": 0,
          "top": 16,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/2-modal-small.png",
          "html": "<div id='pop-bn-small-plain' style='top: 30%; right: 2%;'>" + "This is a front and back form with patient information on page 1 and prescriber " + 
          "information on page 2. All required fields are marked with an asterisks." + "</div>"
        },
        "star-bn-3-v3": {
          "height": 650,
          "width": 950,
          "left": 0,
          "top": 16,  
          "bottom": 0,
          "right": 0, 
          "background-img": "img/3-modal-small.png",
          "html": "<div id='pop-bn-small-plain' style='top: 30%; right: 2%;'>" + "This form requires 2 patient signatures - one for sharing patient health " + 
          "information with Insmed and our 3rd party partners, and another for enrolling the services offered by the program." + "</div>"
        },
        "star-bn-1-patient-v": {
          "height": 650,
          "width": 1328,
          "left": 0,
          "top": 10,  
          "bottom": 0,
          "right": 0, 
          "img": "img/PLACEHOLDER QR.png", 
          "background-img": "img/1-modal-large-plain.png",
          "html": "<div id='pop-bn-large-plain' style='top: 2%;padding:19px;font-size:32px;'>" + "The Welcome Pack arrives to the patient's home shortly after program enrollment with personalized information about their Arikares Coordinator. It provides useful information about resource ARIKAYCE, NTM, and the Arikares program." +
          "<img style='margin: 0 auto; box-shadow: none; display: flex; justify-content: center; align-items: center; width:82%; margin-top: 15px;' src='img/Branch 2-Patient Experience-Popover1.png'>" + "</div>" + "<img id='qr-code' src='img/Workshop3_7 - SL307.png'>"
        },
      };

     